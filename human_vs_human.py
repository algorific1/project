import numpy as np
import pygame
SIZE = 800
ROW, COL = 8, 8
SQUARE_SIZE = SIZE // ROW
SPACE = 20
black = ['B', '', 'B', '', 'B', '', 'B', '', 'B']
empty = ['', '', '', '', '', '', '', '']
white = ['', 'W', '', 'W', '', 'W', '', 'W', '']
opp = {'W': ['B', 'P'], 'B': ['W', 'Y'], 'Y': ['B', 'P'], 'P': ['W', 'Y']}
color = {'W': "white", 'B': "red", 'Y': "yellow", 'P': "purple"}
pygame.init()

class checkers:
    def __init__(self):
        self.running = True
        self.screen = pygame.display.set_mode((SIZE, SIZE))
        self.clock = pygame.time.Clock()
        self.mandatory = set()
        self.board = np.array([black[:-1], black[1:], black[:-1],empty, empty, white[:-1], white[1:], white[:-1]])
        self.current_player = (-1, -1)
        self.current_color = 'W'
        self.killed = False
        for row in range(0, 8):
            for col in range(0, 8):
                if (row + col) % 2 == 1:
                    pygame.draw.rect(self.screen, "white", (col * SQUARE_SIZE, row * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))
                else:
                    self.paint_green((row, col))
                    if self.board[row][col] == 'W':
                        pygame.draw.circle(self.screen, "white",((col * SQUARE_SIZE) + SQUARE_SIZE // 2, (row * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)
                    elif self.board[row][col] == 'B':
                        pygame.draw.circle(self.screen, "red",((col * SQUARE_SIZE) + SQUARE_SIZE // 2, (row * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)
        self.can_goto = [[[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[(3, 1)], [], [(3, 1), (3, 3)], [], [(3, 3), (3, 5)], [], [(3, 5), (3, 7)], []], [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[], [(4, 0), (4, 2)], [], [(4, 2), (4, 4)], [], [(4, 4), (4, 6)], [], [(4, 6)]] , [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]]]

#########################################################################################################################################
    def paint_green(self, pos):
        pygame.draw.rect(self.screen, "green", (pos[1] * SQUARE_SIZE, pos[0] * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))

##########################################################################################################################################

    def update_cangoto(self):
        def can_go_to(row, col):
            can_goto = []
            piece_color = self.board[row][col]
            if piece_color != 'W':
                if col < 7 and row < 7 and self.board[row + 1][col + 1] == '':
                    can_goto.append((row + 1, col + 1))

                elif col < 6 and row < 6 and (self.board[row + 1][col + 1] in opp[self.board[row][col]]) and self.board[row + 2][col + 2] == '':
                    can_goto.append((row + 2, col + 2))
                    self.mandatory.add((row,col))

                if col > 0 and row < 7 and self.board[row + 1][col - 1] == '':
                   can_goto.append((row + 1, col - 1))

                elif col > 1 and row < 6 and (self.board[row + 1][col - 1] in opp[self.board[row][col]]) and self.board[row + 2][col - 2] == '':
                   can_goto.append((row + 2, col - 2))
                   self.mandatory.add((row,col))

            if piece_color != 'B':
                if col < 7 and row > 0 and self.board[row - 1][col + 1] == '':
                    can_goto.append((row - 1, col + 1))

                elif col < 6 and row > 1 and (self.board[row - 1][col + 1] in opp[self.board[row][col]]) and self.board[row - 2][col + 2] == '':
                    can_goto.append((row - 2, col + 2))
                    self.mandatory.add((row,col))

                if col > 0 and row > 0 and self.board[row - 1][col - 1] == '':
                    can_goto.append((row - 1, col - 1))

                elif col > 1 and row > 1 and (self.board[row - 1][col - 1] in opp[self.board[row][col]]) and self.board[row - 2][col - 2] == '':
                    can_goto.append((row - 2, col - 2))
                    self.mandatory.add((row,col))

            return can_goto


        self.mandatory = set()
        can_goto_list = []

        if self.killed:
            current_cangoto = can_go_to(self.current_player[0], self.current_player[1])
            if len(self.mandatory) != 0:
                for row in range(8):
                    can_goto_list.append([])
                    for col in range(8):
                        can_goto_list[row].append([])
                        if row == self.current_player[0] and col == self.current_player[1]:
                            can_goto_list[row][col] = (current_cangoto)
        if (not self.killed) or (not len(self.mandatory)):
            for row in range(0, 8):
                can_goto_list.append([])
                for col in range(0, 8):
                    can_goto_list[row].append([])
                    if self.board[row][col] in opp[self.current_color]:
                        can_goto_list[row][col] = can_go_to(row, col)


        return can_goto_list


########################################################################################################################################

    def move(self, pos1, pos2):

        if len(self.mandatory) != 0 and (pos1 not in self.mandatory or (abs(pos1[0] - pos2[0]) != 2 and abs(pos1[1] - pos2[1]) != 2)):
            return
        print (pos1, pos2)

        if pos1 != None and pos2 in self.can_goto[pos1[0]][pos1[1]]:
            self.current_color = self.board[pos1[0]][pos1[1]]

            def swap(pos1, pos2):
                if pos2[0] in (0, 7) and self.board[pos1[0]][pos1[1]] == 'W':
                    self.board[pos1[0], pos1[1]] = 'Y'
                elif pos2[0] in (0, 7) and self.board[pos1[0]][pos1[1]] == 'B':
                    self.board[pos1[0], pos1[1]] = 'P'

                self.board[pos2[0]][pos2[1]], self.board[pos1[0]][pos1[1]] = self.board[pos1[0]][pos1[1]], ''
                self.current_player = pos2
                self.paint_green(pos1)
                pygame.draw.circle(self.screen, color[self.board[pos2[0]][pos2[1]]], ((pos2[1] * SQUARE_SIZE) + SQUARE_SIZE // 2, (pos2[0] * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)


            if abs(pos1[0] - pos2[0]) == 1 and abs(pos1[1] - pos2[1]) == 1:
                self.killed = False
                swap(pos1, pos2)

            elif abs(pos1[0] - pos2[0]) == 2 and abs(pos1[1] - pos2[1]) == 2:
                 self.killed = True
                 swap(pos1, pos2)
                 self.paint_green( ((pos1[0] + pos2[0]) // 2, (pos1[1] + pos2[1]) // 2) )
                 self.board[(pos1[0] + pos2[0]) // 2][(pos1[1] + pos2[1]) // 2] = ''

            self.can_goto = self.update_cangoto()
            if self.game_end():
                self.running = False

########################################################################################################################################

    def game_end(self):
        if ('W' not in self.board and 'Y' not in self.board) or ('B' not in self.board and 'P' not in self.board) or len(self.can_goto) == 0:
            self.running = False
            print (f"GAME WON BY : {self.current_color}")


#######################################################################################################################################


    def execute(self):
        selected_piece = None
        while self.running:
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    self.running = False

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x, y = pygame.mouse.get_pos()
                    row, col = y // SQUARE_SIZE , x // SQUARE_SIZE

                    if selected_piece is None and self.board[row][col] != '':
                        selected_piece = (row, col)

                    else:
                        self.current_player = selected_piece
                        self.move(selected_piece, (row, col))
                        selected_piece = None
                pygame.display.flip()
                self.clock.tick(60)

###################################################################################################################################

obj = checkers()
obj.execute()
for line in obj.board:
    print (line)
