import numpy as np
import pygame
from copy import deepcopy

SIZE = 800
ROW, COL = 8, 8
SQUARE_SIZE = SIZE // ROW
SPACE = 20
DEPTH = 4
NEG_INF, POS_INF = -100000, 100000
black = ['B', '', 'B', '', 'B', '', 'B', '', 'B']
empty = ['', '', '', '', '', '', '', '']
white = ['', 'W', '', 'W', '', 'W', '', 'W', '']
opp = {'W': ['B', 'P'], 'B': ['W', 'Y'], 'Y': ['B', 'P'], 'P': ['W', 'Y']}
color = {'W': "white", 'B': "red", 'Y': "yellow", 'P': "purple"}
WHITE_STARTS = [[[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[], [], [], [], [], [], [], []], [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[], [(4,0),(4,2)], [], [(4,2),(4,4)], [], [(4,4),(4,6)], [], [(4,6)]] , [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]]]
pygame.init()



def evaluate_board (board) -> int:
    score = 0
    change_in_score = {'W': -100, 'B': 100, 'Y': -200, 'P': 200, '': 0}
    for row in range(8):
        for col in range(8):
            piece = board[row][col]
            score += change_in_score[piece]
            if (row >= 2 and row <= 5) and (col >= 2 and col <= 5):
                if (piece == 'W' or piece == 'Y'):
                    score -= 25
                elif piece == 'B' or piece == 'P':
                    score += 25
    return score




def minimax (board, killed, prev_player, current_player, depth :int, maximizing_player :bool):
    possible_pos1, zigzag, killed, game_over = update_possible_moves(board, killed, current_player)
    if depth == 0 or game_over:
        return evaluate_board(board), (prev_player, current_player)
  
    if maximizing_player:
        max_eval, max_dict = NEG_INF, dict()
        best_move = ((0,0), (0,0))
        for row in range(8):
            for col in range(8):
                possible_pos2 = possible_pos1[row][col]
                if len(possible_pos2) != 0:
                    pos1 = (row, col)
                    for pos2 in possible_pos2:
                        temp_board, killed = move_in_board(deepcopy(board), pos1, pos2)
                        evalu,_ = minimax(temp_board, killed, pos1,  pos2, depth - 1, False)
                        max_eval = max(max_eval, evalu)
                        if max_eval == evalu:
                            max_dict[max_eval] = (pos1, pos2)
                            best_move = (pos1, pos2)
        final_max, final_best_move = max_eval, best_move
        if len(max_dict) != 0:
            for key in max_dict.keys():
                if key > final_max:
                    final_max, final_best_move = key, max_dict[key]
        return final_max, final_best_move
    else:
        min_eval, min_dict = POS_INF, dict()
        best_move = ((0,0), (0,0))
        for row in range(8):
            for col in range(8):
                possible_pos2 = possible_pos1[row][col]
                if len(possible_pos2) != 0:
                    pos1 = (row, col)
                    for pos2 in possible_pos2:
                        temp_board, killed = move_in_board(deepcopy(board), pos1, pos2)
                        evalu,_ = minimax(temp_board, killed,pos1,  pos2, depth - 1, True)
                        min_eval = min(min_eval, evalu)
                        if min_eval == evalu:
                            best_move = (pos1, pos2)
                            min_dict[min_eval] = best_move
        final_min, final_best_move = min_eval, best_move
        if len(min_dict) != 0:
            for key in min_dict.keys():
                if key > final_min:
                    final_min, final_best_move = key, min_dict[key]
        return final_min, final_best_move






def update_possible_moves (board, killed, current_player):
    current_color, mandatory = board[current_player[0]][current_player[1]], set()
    def can_go_to(row, col):
        can_goto = []
        piece_color = board[row][col]
        if piece_color != 'W':
            if col < 7 and row < 7 and board[row + 1][col + 1] == '':
                can_goto.append((row + 1, col + 1))
            elif col < 6 and row < 6 and (board[row + 1][col + 1] in opp[board[row][col]]) and board[row + 2][col + 2] == '':
                can_goto.append((row + 2, col + 2))
                mandatory.add((row,col))
            if col > 0 and row < 7 and board[row + 1][col - 1] == '':
               can_goto.append((row + 1, col - 1))
            elif col > 1 and row < 6 and (board[row + 1][col - 1] in opp[board[row][col]]) and board[row + 2][col - 2] == '':
                can_goto.append((row + 2, col - 2))
                mandatory.add((row,col))

        if piece_color != 'B':
            if col < 7 and row > 0 and board[row - 1][col + 1] == '':
                can_goto.append((row - 1, col + 1))
            elif col < 6 and row > 1 and (board[row - 1][col + 1] in opp[board[row][col]]) and board[row - 2][col + 2] == '':
                can_goto.append((row - 2, col + 2))
                mandatory.add((row,col))
            if col > 0 and row > 0 and board[row - 1][col - 1] == '':
                can_goto.append((row - 1, col - 1))
            elif col > 1 and row > 1 and (board[row - 1][col - 1] in opp[board[row][col]]) and board[row - 2][col - 2] == '':
                can_goto.append((row - 2, col - 2))
                mandatory.add((row,col))
        return can_goto

    can_goto_list, zigzag, game_over = [], False, False
    # if zigzag possible, current players can goto positions
    if killed:
        current_cangoto = can_go_to(current_player[0], current_player[1])
        if len(mandatory) != 0:
            zigzag = True
            for row in range(8):
                can_goto_list.append([])
                for col in range(8):
                    can_goto_list[row].append([])
                    if row == current_player[0] and col == current_player[1]:
                        can_goto_list[row][col] = (current_cangoto)
    # if zigzag not possible, next colors can goto positions
    if (not killed) or len(mandatory) == 0:
        for row in range(0, 8):
            can_goto_list.append([])
            for col in range(0, 8):
                can_goto_list[row].append([])
                if board[row][col] in opp[current_color]:
                    can_goto_list[row][col] = can_go_to(row, col)
        if len(mandatory) != 0:
            killed = False
            for row in range(0, 8):
                for col in range(0, 8):
                    if (row, col) not in mandatory:
                        can_goto_list[row][col] = []
                    else:
                        pos1, remove = (row, col), set()
                        for pos2 in can_goto_list[row][col]:
                            if abs(pos1[0] - pos2[0]) != 2 or abs(pos1[1] - pos2[1]) != 2:
                                remove.add(pos2)
                        for pos2 in remove:
                            (can_goto_list[row][col]).remove(pos2)
    cannot_go_anywhere = len([True for ele in can_goto_list if ele == [[],[],[],[],[],[],[],[]]]) == 8
    if ('W' not in board and 'Y' not in board) or ('B' not in board and 'P' not in board) or cannot_go_anywhere:
        game_over = True
    return can_goto_list, zigzag, killed, game_over





def move_in_board (board, pos1, pos2):
    current_color = board[pos1[0]][pos1[1]]
    killed = False
    def swap(pos1, pos2):
        if pos2[0] in (0, 7) and current_color == 'W':
            board[pos1[0], pos1[1]] = 'Y'
        elif pos2[0] in (0, 7) and current_color == 'B':
            board[pos1[0], pos1[1]] = 'P'
        board[pos2[0]][pos2[1]], board[pos1[0]][pos1[1]] = board[pos1[0]][pos1[1]], ''
    if abs(pos1[0] - pos2[0]) == 1 and abs(pos1[1] - pos2[1]) == 1:
        killed = False
        swap(pos1, pos2)
    elif abs(pos1[0] - pos2[0]) == 2 and abs(pos1[1] - pos2[1]) == 2:
        killed = True
        swap(pos1, pos2)
        board[(pos1[0] + pos2[0]) // 2][(pos1[1] + pos2[1]) // 2] = ''
    return board, killed



########################################################################################################################################



class checkers:
    def __init__(self):
        self.running = True
        self.screen = pygame.display.set_mode((SIZE, SIZE))
        self.clock = pygame.time.Clock()
        self.mandatory = set()
        self.board = np.array([black[:-1], black[1:], black[:-1],empty, empty, white[:-1], white[1:], white[:-1]])
        self.can_goto = WHITE_STARTS
        self.current_player = (-1, -1)
        self.current_color = 'W'
        self.killed = False
        self.zigzag = False
        for row in range(0, 8):
            for col in range(0, 8):
                if (row + col) % 2 == 1:
                    pygame.draw.rect(self.screen, "white", (col * SQUARE_SIZE, row * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))
                else:
                    self.paint_green((row, col))
                    if self.board[row][col] == 'W':
                        pygame.draw.circle(self.screen, "white",((col * SQUARE_SIZE) + SQUARE_SIZE // 2, (row * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)
                    elif self.board[row][col] == 'B':
                        pygame.draw.circle(self.screen, "red",((col * SQUARE_SIZE) + SQUARE_SIZE // 2, (row * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)



    def paint_green(self, pos):
        pygame.draw.rect(self.screen, "green", (pos[1] * SQUARE_SIZE, pos[0] * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))



    def move (self, pos1, pos2, LAG):
        if len(self.mandatory) != 0 and (pos1 not in self.mandatory or (abs(pos1[0] - pos2[0]) != 2 and abs(pos1[1] - pos2[1]) != 2)):
            return
        print (pos1, pos2)
        if pos1 != None and pos2 in self.can_goto[pos1[0]][pos1[1]]:
            self.current_color = self.board[pos1[0]][pos1[1]]
            self.board, self.killed = move_in_board(self.board, pos1, pos2)

            self.paint_green(pos1)

            if LAG:
                x1, y1 = pos1[1] * SQUARE_SIZE + SQUARE_SIZE // 2, pos1[0] * SQUARE_SIZE + SQUARE_SIZE // 2
                x2, y2 = pos2[1] * SQUARE_SIZE + SQUARE_SIZE // 2, pos2[0] * SQUARE_SIZE + SQUARE_SIZE // 2

                dx = (x2 - x1)
                dy = (y2 - y1)

                for step in range(0,1):
                    x = x1 + step * dx
                    y = y1 + step * dy

                    pygame.draw.circle(self.screen, color[self.board[pos2[0]][pos2[1]]],(x1,y1) , SQUARE_SIZE // 2 - SPACE)
                    pygame.display.flip()
                    pygame.time.delay(700)
                    pygame.draw.circle(self.screen, "green",(int(x), int(y)), SQUARE_SIZE // 2 - SPACE)
                    pygame.display.flip()

            pygame.draw.circle(self.screen, color[self.board[pos2[0]][pos2[1]]], ((pos2[1] * SQUARE_SIZE) + SQUARE_SIZE // 2, (pos2[0] * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)
            pygame.display.flip()

            self.current_player = pos2
            if self.killed:
                 self.paint_green( ((pos1[0] + pos2[0]) // 2, (pos1[1] + pos2[1]) // 2) )

            self.can_goto, self.zigzag, self.killed, game_over = update_possible_moves(self.board, self.killed, self.current_player)
            if game_over:
                self.running = False
                print (f"GAME WON BY : {self.current_color}")



    def minimax_play(self):
        evalu, best_move = minimax(self.board, self.killed,(0,0), self.current_player, DEPTH, True)
        self.move(best_move[0], best_move[1], True)
        while self.zigzag:
            self.minimax_play()



    def execute (self):
        selected_piece = None
        while self.running:
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    self.running = False

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x, y = pygame.mouse.get_pos()
                    row, col = y // SQUARE_SIZE , x // SQUARE_SIZE

                    if selected_piece is None and self.board[row][col] in ('W', 'Y'):
                        selected_piece = (row, col)
                    else:
                        # Human turn
                        self.current_player = selected_piece
                        self.move(selected_piece, (row, col), False)
                        # Bot turn
                        if selected_piece != None and not self.zigzag:
                            self.minimax_play()
                        selected_piece = None
                pygame.display.flip()
                self.clock.tick(60)







obj = checkers()
obj.execute()
for line in obj.board:
    print (line)

