# Development, Comparison and Analysis of Minimax Algorithm and Reinforcement Learning through Implementation in Checkers game




## Table of contents
* [Introduction](#Introduction)
* [Installation](#Installation)
* [Start](#Start)
* [Our Analysis](#Our Analysis)





## Introduction
### Objective

We developed a two-player checkers game and implemented a Minimax Bot and a Reinforcemnt Learning (Q-Learning) Bot. Subsequently, we conducted a comparative analysis of these bots' performance.

We have attempted to adapted the concepts and analysis methods from research of Santiago Videgain and Pablo Garcia Sanchez (2021) in their paper titled *’Performance Study of Minimax and Reinforcement Learning Agents Playing the Turn-based Game Iwoki'*. We have implement these analysis methods for Minimax and Reinforcement Learning in classic game of checkers.


### Our Implementations Include:

1. A 2 player Checkers game
2. Minimax Bot
3. Reinforcement Learning Bot(Q Learning)
4. Minimax vs RL Game
5. Comparision and Analysis of these two bots against each other, Human player and Random agent





## Installation
Project is created with:
* Python 3.10

```
$ pip install numpy

```
```
$ pip install pygame

```


## Start
To run this project, run The ui_code file

```
$ python3 ui_code.py

```

To train the RL agent against Minimax Bot, run

```
$ python3 rl_training__minimax.py
```

To train the RL agent against RL Bot, run

```
$ python3 rl_training__rl.py
```


*Note: The required training json file needs to be created beforehand with empty dictionary*





## Our Analysis

[**Read about our Learnings, Findings and Analysis Here.**](https://algo-checkers.readthedocs.io/en/latest/)