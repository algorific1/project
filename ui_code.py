import pygame
import sys
import time
import math
pygame.init()
import random
import json

learning_rate = 0.4
discount_rate = 0.9
Q_table = dict()

SIZE = 800
ROW, COL = 8, 8
SQUARE_SIZE = SIZE // ROW
DARK_WOOD = (115, 80, 50)
LIGHT_WOOD = (240, 217, 181)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BUTTON_COLOR = (181, 204, 240)
BUTTON_PRESSED_COLOR = (50, 85, 115)
LAG = False

screen = pygame.display.set_mode((SIZE, SIZE))
pygame.display.set_caption('Checkers Game')

title_font = pygame.font.SysFont(None, 72, bold=True)
button_font = pygame.font.Font(None, 36)

# sprite_image = pygame.image.load('welcome.png')
# sprite_image = pygame.transform.scale(sprite_image, (SQUARE_SIZE, SQUARE_SIZE))

welcome_bg_image = pygame.image.load('images/bg5.png')
welcome_bg_image = pygame.transform.scale(welcome_bg_image, (SIZE, SIZE))

def draw_board():
    for row in range(ROW):
        for col in range(COL):
            color = LIGHT_WOOD if (row + col) % 2 == 0 else DARK_WOOD
            pygame.draw.rect(screen, color, (col * SQUARE_SIZE, row * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))
    pygame.draw.rect(screen, BLACK, (0, 0, SIZE, SIZE), 3)

def welcome_screen():
    screen.blit(welcome_bg_image, (0, 0))
    colors = [pygame.Color("red"), pygame.Color("green"), pygame.Color("blue"), pygame.Color("yellow"), pygame.Color("orange")]
    for i, color in enumerate(colors):
        pygame.draw.circle(screen, color, (SIZE // 2 + i * 20, SIZE // 1.2), 10)

    # for i in range(8):
    #     screen.blit(sprite_image, (i * SQUARE_SIZE, SIZE // 1.5))
    #     pygame.display.flip()
    #     time.sleep(0.4)

    pygame.display.flip()
    time.sleep(3)

def home_page(animation_step):
    screen.fill(BLACK)
    draw_board()

    start_row = 0
    start_col = 0
    piece_x = (start_col + (animation_step % ROW)) * SQUARE_SIZE + SQUARE_SIZE // 2
    piece_y = (start_row + (animation_step % COL)) * SQUARE_SIZE + SQUARE_SIZE // 2
    pygame.draw.circle(screen, WHITE, (piece_x, piece_y), SQUARE_SIZE // 3)

    title_text = title_font.render('Welcome to Checkers Game', True, '#BF40BF')
    screen.blit(title_text, (SIZE // 2 - title_text.get_width() // 2, SIZE // 3.5))

    rules_button = pygame.Rect(SIZE // 4, SIZE // 2, SIZE // 2, 50)
    play_button = pygame.Rect(SIZE // 4, SIZE // 2 + 100, SIZE // 2, 50)

    draw_animated_button(rules_button, "Click to see rules", animation_step)
    draw_animated_button(play_button, "Play now", animation_step)

    pygame.display.flip()

    return rules_button, play_button

def draw_animated_button(rect, text, animation_step):
    pulsate_scale = 5
    pulsate_speed = 0.1
    pulsate_offset = int(pulsate_scale * (1 + math.sin(animation_step * pulsate_speed)))
    color = BUTTON_COLOR if not rect.collidepoint(pygame.mouse.get_pos()) else BUTTON_PRESSED_COLOR
    pygame.draw.rect(screen, color, (rect.x - pulsate_offset, rect.y - pulsate_offset, rect.width + 2 * pulsate_offset, rect.height + 2 * pulsate_offset), border_radius=20)

    button_text = button_font.render(text, True, BLACK if color == BUTTON_COLOR else WHITE)
    screen.blit(button_text, (rect.x + (rect.width - button_text.get_width()) // 2, rect.y + (rect.height - button_text.get_height()) // 2))

def winning_animation(message):
    screen.fill("blue")
    winner_text = title_font.render(message, True, "orange")
    screen.blit(winner_text, (SIZE // 2 - winner_text.get_width() // 2, SIZE // 3))
    pygame.display.flip()
    time.sleep(3) 
    pygame.display.flip()


def game_mode_selection():
    screen.fill(BLACK)

    title_text = title_font.render('Select Game Mode', True, LIGHT_WOOD)
    screen.blit(title_text, (SIZE // 2 - title_text.get_width() // 2, SIZE // 5))

    player_button = pygame.Rect(SIZE // 4, SIZE // 3, SIZE // 2, 50)
    minimax_button = pygame.Rect(SIZE // 4, SIZE // 3 + 100, SIZE // 2, 50)
    rl_button = pygame.Rect(SIZE // 4, SIZE // 3 + 200, SIZE // 2, 50)
    bot_button = pygame.Rect(SIZE // 4, SIZE // 3 + 300, SIZE // 2, 50)
    back_button = pygame.Rect(SIZE // 4, SIZE // 3 + 400, SIZE // 2, 50)

    draw_animated_button(player_button, "2 Player Game", True)
    draw_animated_button(minimax_button, "Human vs Minimax Bot", True)
    draw_animated_button(rl_button, "Human vs RL Bot", True)
    draw_animated_button(bot_button, "Minimax Bot vs RL Bot", True)
    draw_animated_button(back_button, "Back to Home", True)

    pygame.display.flip()

    return player_button, minimax_button, rl_button, bot_button, back_button

def rules_page():
    screen.fill(BLACK)
    rules_text = [
        "Checkers Rules:",
        "1. Checker piece always moves one diagonal space forward.",
        "2. Capture: kill opponent's piece by jumping over it.",
        "3. Mandatory kill: Must capture if you have the opportunity.",
        "4. Keep capturing with same piece until you cannot.",
        "5. When a piece reaches the opposite end, it becomes a KING which can now move backword also."
    ]

    for i, line in enumerate(rules_text):
        text = button_font.render(line, True, LIGHT_WOOD)
        screen.blit(text, (SIZE // 10, SIZE // 10 + i * 40))

    back_button = pygame.Rect(SIZE // 4, SIZE // 2 + 200, SIZE // 2, 50)
    draw_animated_button(back_button, "Back to Home", False)

    pygame.display.flip()

    return back_button


import numpy as np
import pygame
from copy import deepcopy

SIZE = 800
ROW, COL = 8, 8
SQUARE_SIZE = SIZE // ROW
SPACE = 20

DEPTH = 4
NEG_INF, POS_INF = -100000, 100000
black = ['B', '', 'B', '', 'B', '', 'B', '', 'B']
empty = ['', '', '', '', '', '', '', '']
white = ['', 'W', '', 'W', '', 'W', '', 'W', '']
opp = {'W': ['B', 'P'], 'B': ['W', 'Y'], 'Y': ['B', 'P'], 'P': ['W', 'Y']}
color = {'W': "white", 'B': "red", 'Y': "yellow", 'P': "purple"}
WHITE_STARTS = [[[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[], [], [], [], [], [], [], []], [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[], [(4,0),(4,2)], [], [(4,2),(4,4)], [], [(4,4),(4,6)], [], [(4,6)]] , [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]]]
BLACK_STARTS = [[[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[(3, 1)], [], [(3, 1), (3, 3)], [], [(3, 3), (3, 5)], [], [(3, 5), (3, 7)], []], [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]], [[], [], [], [], [], [], [], []] , [[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[]]]
pygame.init()


with open("training_files/training_datap1.json", 'r') as f:
    Q_table = json.load(f)


def board_to_str(state):
    str_state = ''
    for row in range(8):
        for col in range(8):
            if state[row][col] == '':
                str_state += ' '
            else:
                str_state += state[row][col]
    return str_state



def evaluate_board (board) -> int:
    score = 0
    change_in_score = {'W': -100, 'B': 100, 'Y': -200, 'P': 200, '': 0}
    for row in range(8):
        for col in range(8):
            piece = board[row][col]
            score += change_in_score[piece]
            if (row >= 2 and row <= 5) and (col >= 2 and col <= 5):
                if (piece == 'W' or piece == 'Y'):
                    score -= 25
                elif piece == 'B' or piece == 'P':
                    score += 25
    return score


def evaluate_reward(board, current_player, killed):
    reward = 0
    if killed : reward += 1
    return reward


def minimax (board, killed, prev_player, current_player, depth :int, maximizing_player :bool):
    possible_pos1, zigzag, killed, game_over = update_possible_moves(board, killed, current_player)
    if depth == 0 or game_over:
        return evaluate_board(board), (prev_player, current_player)
  
    if maximizing_player:
        max_eval, max_dict = NEG_INF, dict()
        best_move = ((0,0), (0,0))
        for row in range(8):
            for col in range(8):
                possible_pos2 = possible_pos1[row][col]
                if len(possible_pos2) != 0:
                    pos1 = (row, col)
                    for pos2 in possible_pos2:
                        temp_board, killed = move_in_board(deepcopy(board), pos1, pos2)
                        evalu,_ = minimax(temp_board, killed, pos1,  pos2, depth - 1, False)
                        max_eval = max(max_eval, evalu)
                        if max_eval == evalu:
                            max_dict[max_eval] = (pos1, pos2)
                            best_move = (pos1, pos2)
        final_max, final_best_move = max_eval, best_move
        if len(max_dict) != 0:
            for key in max_dict.keys():
                if key > final_max:
                    final_max, final_best_move = key, max_dict[key]
        return final_max, final_best_move
    else:
        min_eval, min_dict = POS_INF, dict()
        best_move = ((0,0), (0,0))
        for row in range(8):
            for col in range(8):
                possible_pos2 = possible_pos1[row][col]
                if len(possible_pos2) != 0:
                    pos1 = (row, col)
                    for pos2 in possible_pos2:
                        temp_board, killed = move_in_board(deepcopy(board), pos1, pos2)
                        evalu,_ = minimax(temp_board, killed,pos1,  pos2, depth - 1, True)
                        min_eval = min(min_eval, evalu)
                        if min_eval == evalu:
                            best_move = (pos1, pos2)
                            min_dict[min_eval] = best_move
        final_min, final_best_move = min_eval, best_move
        if len(min_dict) != 0:
            for key in min_dict.keys():
                if key > final_min:
                    final_min, final_best_move = key, min_dict[key]
        return final_min, final_best_move




def update_possible_moves (board, killed, current_player):
    current_color, mandatory = board[current_player[0]][current_player[1]], set()
    def can_go_to(row, col):
        can_goto = []
        piece_color = board[row][col]
        if piece_color != 'W':
            if col < 7 and row < 7 and board[row + 1][col + 1] == '':
                can_goto.append((row + 1, col + 1))
            elif col < 6 and row < 6 and (board[row + 1][col + 1] in opp[board[row][col]]) and board[row + 2][col + 2] == '':
                can_goto.append((row + 2, col + 2))
                mandatory.add((row,col))
            if col > 0 and row < 7 and board[row + 1][col - 1] == '':
               can_goto.append((row + 1, col - 1))
            elif col > 1 and row < 6 and (board[row + 1][col - 1] in opp[board[row][col]]) and board[row + 2][col - 2] == '':
                can_goto.append((row + 2, col - 2))
                mandatory.add((row,col))

        if piece_color != 'B':
            if col < 7 and row > 0 and board[row - 1][col + 1] == '':
                can_goto.append((row - 1, col + 1))
            elif col < 6 and row > 1 and (board[row - 1][col + 1] in opp[board[row][col]]) and board[row - 2][col + 2] == '':
                can_goto.append((row - 2, col + 2))
                mandatory.add((row,col))
            if col > 0 and row > 0 and board[row - 1][col - 1] == '':
                can_goto.append((row - 1, col - 1))
            elif col > 1 and row > 1 and (board[row - 1][col - 1] in opp[board[row][col]]) and board[row - 2][col - 2] == '':
                can_goto.append((row - 2, col - 2))
                mandatory.add((row,col))
        return can_goto

    can_goto_list, zigzag, game_over = [], False, False
    # if zigzag possible, current players can goto positions
    if killed:
        current_cangoto = can_go_to(current_player[0], current_player[1])
        if len(mandatory) != 0:
            zigzag = True
            for row in range(8):
                can_goto_list.append([])
                for col in range(8):
                    can_goto_list[row].append([])
                    if row == current_player[0] and col == current_player[1]:
                        can_goto_list[row][col] = (current_cangoto)
    # if zigzag not possible, next colors can goto positions
    if (not killed) or len(mandatory) == 0:
        for row in range(0, 8):
            can_goto_list.append([])
            for col in range(0, 8):
                can_goto_list[row].append([])
                if board[row][col] in opp[current_color]:
                    can_goto_list[row][col] = can_go_to(row, col)
        if len(mandatory) != 0:
            killed = False
            for row in range(0, 8):
                for col in range(0, 8):
                    if (row, col) not in mandatory:
                        can_goto_list[row][col] = []
                    else:
                        pos1, remove = (row, col), set()
                        for pos2 in can_goto_list[row][col]:
                            if abs(pos1[0] - pos2[0]) != 2 or abs(pos1[1] - pos2[1]) != 2:
                                remove.add(pos2)
                        for pos2 in remove:
                            (can_goto_list[row][col]).remove(pos2)
    cannot_go_anywhere = len([True for ele in can_goto_list if ele == [[],[],[],[],[],[],[],[]]]) == 8
    if ('W' not in board and 'Y' not in board) or ('B' not in board and 'P' not in board) or cannot_go_anywhere:
        game_over = True
    return can_goto_list, zigzag, killed, game_over




def valid_moves(board, killed, current_player):
    valid_moves = []
    can_goto_list, zigzag, killed, game_over = update_possible_moves(board, killed, current_player)
    for row in range(8):
        for col in range(8):
            if len(can_goto_list[row][col]) != 0:
                for pos2 in can_goto_list[row][col]:
                    valid_moves.append(str(row) + str(col) + str(pos2[0]) + str(pos2[1]))
    return valid_moves



def move_in_board (board, pos1, pos2):
    current_color = board[pos1[0]][pos1[1]]
    killed = False
    def swap(pos1, pos2):
        if pos2[0] in (0, 7) and current_color == 'W':
            board[pos1[0], pos1[1]] = 'Y'
        elif pos2[0] in (0, 7) and current_color == 'B':
            board[pos1[0], pos1[1]] = 'P'
        board[pos2[0]][pos2[1]], board[pos1[0]][pos1[1]] = board[pos1[0]][pos1[1]], ''
    if abs(pos1[0] - pos2[0]) == 1 and abs(pos1[1] - pos2[1]) == 1:
        killed = False
        swap(pos1, pos2)
    elif abs(pos1[0] - pos2[0]) == 2 and abs(pos1[1] - pos2[1]) == 2:
        killed = True
        swap(pos1, pos2)
        board[(pos1[0] + pos2[0]) // 2][(pos1[1] + pos2[1]) // 2] = ''
    return board, killed

########################################################################################################################################

class checkers:
    def __init__(self):
        self.running = True
        self.screen = pygame.display.set_mode((SIZE, SIZE))
        self.clock = pygame.time.Clock()
        self.mandatory = set()
        self.board = np.array([black[:-1], black[1:], black[:-1],empty, empty, white[:-1], white[1:], white[:-1]])
        self.can_goto = WHITE_STARTS
        self.current_player = (0, 0)
        self.current_color = 'W'
        self.killed = False
        self.zigzag = False
        self.all_moves = []
        self.epsilon = 0
        self.init_graphics()



    def init_graphics(self):
        for row in range(0, 8):
            for col in range(0, 8):
                if (row + col) % 2 == 1:
                    pygame.draw.rect(self.screen, "white", (col * SQUARE_SIZE, row * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))
                else:
                    self.paint_green((row, col))
                    if self.board[row][col] == 'W':
                        pygame.draw.circle(self.screen, "white",((col * SQUARE_SIZE) + SQUARE_SIZE // 2, (row * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)
                    elif self.board[row][col] == 'B':
                        pygame.draw.circle(self.screen, "red",((col * SQUARE_SIZE) + SQUARE_SIZE // 2, (row * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)



    def paint_green(self, pos):
        pygame.draw.rect(self.screen, "green", (pos[1] * SQUARE_SIZE, pos[0] * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))


    

    def update_graphics(self, pos1, pos2, LAG):

        self.paint_green(pos1)

        if LAG:
            x1, y1 = pos1[1] * SQUARE_SIZE + SQUARE_SIZE // 2, pos1[0] * SQUARE_SIZE + SQUARE_SIZE // 2
            x2, y2 = pos2[1] * SQUARE_SIZE + SQUARE_SIZE // 2, pos2[0] * SQUARE_SIZE + SQUARE_SIZE // 2

            dx = (x2 - x1)
            dy = (y2 - y1) 

            for step in range(0,1):
                x = x1 + step * dx
                y = y1 + step * dy

            pygame.draw.circle(self.screen, color[self.board[pos2[0]][pos2[1]]],(int(x), int(y)) , SQUARE_SIZE // 2 - SPACE)
            pygame.display.flip()
            pygame.time.delay(400)
            pygame.draw.circle(self.screen, "green",(int(x), int(y)), SQUARE_SIZE // 2 - SPACE)
            pygame.display.flip()

        pygame.draw.circle(self.screen, color[self.board[pos2[0]][pos2[1]]], ((pos2[1] * SQUARE_SIZE) + SQUARE_SIZE // 2, (pos2[0] * SQUARE_SIZE + SQUARE_SIZE // 2)), SQUARE_SIZE // 2 - SPACE)
        pygame.display.flip()
        if self.killed:
            self.paint_green( ((pos1[0] + pos2[0]) // 2, (pos1[1] + pos2[1]) // 2) )


    def move (self, pos1, pos2, LAG):
        if pos1 != None and pos2 in self.can_goto[pos1[0]][pos1[1]]:
            self.current_color = self.board[pos1[0]][pos1[1]]
            self.board, self.killed = move_in_board(self.board, pos1, pos2)
            self.update_graphics(pos1, pos2, LAG)
            self.current_player = pos2
            self.can_goto, self.zigzag, self.killed, game_over = update_possible_moves(self.board, self.killed, self.current_player)
            if game_over:
                if self.current_color in ('W', 'Y'):
                    message = "GAME WON BY : WHITE"
                else:
                    message = "GAME WON BY : RED"
                winning_animation(message)
                
                self.running = False
                print (f"GAME WON BY : {self.current_color}")
                pygame.display.flip()


    def choose_action(self, state):
        str_state = board_to_str(state)
        if np.random.rand() < self.epsilon or str_state not in Q_table.keys():
            return  random.choice(valid_moves(state, self.killed, self.current_player))
        else:
            max_q, best_action = -10000, 0
            for move in Q_table[str_state].keys():
                if Q_table[str_state][move] >= max_q:
                    max_q = Q_table[str_state][move]
                    best_action = move
            return best_action



    def learn(self, current_state, str_current_state, action, reward, str_next_state):
        if str_current_state not in Q_table.keys() or action not in Q_table[str_current_state]:
            Q_table[str_current_state] = dict()
            for action in valid_moves(deepcopy(current_state), deepcopy(self.killed), deepcopy(self.current_player)):
                Q_table[str_current_state][action] = 0

        next_best_score = 0
        if str_next_state in Q_table:
            for act in Q_table[str_next_state]:
                next_best_score = max(Q_table[str_next_state][act], next_best_score)

        Q_table[str_current_state][action] += (learning_rate * (reward + discount_rate * next_best_score - Q_table[str_current_state][action]))



    def rl_play(self):
        if len(valid_moves(deepcopy(self.board), self.killed, self.current_player)) == 0:
            return
        str_state = board_to_str(deepcopy(self.board))
        action = self.choose_action(deepcopy(self.board))
        action_tup = ((int(action[0]), int(action[1])), (int(action[2]), int(action[3])))

        next_state, killed = move_in_board(deepcopy(self.board), action_tup[0], action_tup[1])
        str_next_state = board_to_str(next_state)

        reward = evaluate_reward(next_state, action_tup[0], killed)
        self.learn(deepcopy(self.board), deepcopy(str_state), action, reward, str_next_state)

        self.current_player = action_tup[0]
        self.move(action_tup[0], action_tup[1], True)
        self.all_moves.append((deepcopy(str_state), action))

        if self.zigzag and self.running:
            self.rl_play()



    def minimax_play(self):
        evalu, best_move = minimax(self.board, self.killed,(0,0), self.current_player, DEPTH, True)
        
        self.move(best_move[0], best_move[1], True)
        while self.zigzag:
            self.minimax_play()


    def execute (self, mode1, mode2):
        human_piece_set = ('W', 'Y')
        if mode1 == "Human" and mode2 == "Human": human_piece_set = ('W', 'Y', 'B', 'P')

        selected_piece = None
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif mode1 == "Minimax Bot":
                    self.minimax_play()
                    pygame.time.delay(1000)
                    self.rl_play()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x, y = pygame.mouse.get_pos()
                    row, col = y // SQUARE_SIZE , x // SQUARE_SIZE
                    if selected_piece is None and self.board[row][col] in human_piece_set:
                        selected_piece = (row, col)
                    else:
                        if mode1 == "Human":
                            self.current_player = selected_piece
                            self.move(selected_piece, (row, col), False)
                            if mode2 == "Minimax Bot" or mode2 == "RL Bot":
                                if selected_piece != None and not self.zigzag and self.running:
                                    if mode2 == "Minimax Bot" :  self.minimax_play()
                                    elif mode2 == "RL Bot" : self.rl_play()
                    
                        selected_piece = None
                pygame.display.flip()
                self.clock.tick(60)


def start(mode1, mode2 : str) :
    obj = checkers()
    obj.execute(mode1, mode2)


def play():
    in_home_page = False
    in_rules_page = False
    in_game_mode = False
    animation_step = 0

    welcome_screen()
    in_home_page = True

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if in_home_page:
                    rules_button, play_button = home_page(animation_step)
                    if rules_button.collidepoint(event.pos):
                        draw_animated_button(rules_button, "Click to see rules", True)
                        pygame.display.flip()
                        time.sleep(0.1)
                        draw_animated_button(rules_button, "Click to see rules", False)
                        in_home_page = False
                        in_rules_page = True
                    elif play_button.collidepoint(event.pos):
                        draw_animated_button(play_button, "Play now", True)
                        pygame.display.flip()
                        time.sleep(0.1)
                        draw_animated_button(play_button, "Play now", False)
                        in_home_page = False
                        in_game_mode = True
                elif in_rules_page:
                    back_button = rules_page()
                    if back_button.collidepoint(event.pos):
                        draw_animated_button(back_button, "Back to Home", True)
                        pygame.display.flip()
                        time.sleep(0.1)
                        draw_animated_button(back_button, "Back to Home", False)
                        in_home_page = True
                        in_rules_page = False
                elif in_game_mode:
                    if back_button.collidepoint(event.pos):
                        draw_animated_button(rules_button, "2 Player Game", True)
                        pygame.display.flip()
                        time.sleep(0.1)
                        draw_animated_button(rules_button, "2 Player Game", False)
                        in_game_mode = False
                        in_home_page = True

                    else:
                        if player_button.collidepoint(event.pos):
                            draw_animated_button(rules_button, "2 Player Game", True)
                            pygame.display.flip()
                            time.sleep(0.1)
                            draw_animated_button(rules_button, "2 Player Game", False)
                            mode1 = "Human"
                            mode2 = "Human"
                        elif minimax_button.collidepoint(event.pos):
                            draw_animated_button(rules_button, "2 Player Game", True)
                            pygame.display.flip()
                            time.sleep(0.1)
                            draw_animated_button(rules_button, "2 Player Game", False)
                            mode1= "Human"
                            mode2 = "Minimax Bot"
                        elif rl_button.collidepoint(event.pos):
                            draw_animated_button(rules_button, "2 Player Game", True)
                            pygame.display.flip()
                            time.sleep(0.1)
                            draw_animated_button(rules_button, "2 Player Game", False)
                            mode1 = "Human"
                            mode2 = "RL Bot"
                        elif bot_button.collidepoint(event.pos):
                            draw_animated_button(rules_button, "Minimax Bot vs RL Bot", True)
                            pygame.display.flip()
                            time.sleep(0.1)
                            draw_animated_button(rules_button, "Minimax Bot vs RL Bot", False)
                            mode1 = "Minimax Bot"
                            mode2 = "RL Bot"


                        start(mode1, mode2)
                        in_game_mode = True

        if in_home_page:
            home_page(animation_step)
            animation_step += 1
            time.sleep(1)

        elif in_rules_page:
            rules_page()

        elif in_game_mode:
            player_button, minimax_button, rl_button, bot_button, back_button = game_mode_selection()

        pygame.display.update()


play()
